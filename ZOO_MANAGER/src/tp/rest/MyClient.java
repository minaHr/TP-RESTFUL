package tp.rest;

import java.util.Scanner;
import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

public class MyClient {
	private Service service;
	private JAXBContext jc;

	private static final QName qname = new QName("", "");
	private static final String url = "http://127.0.0.1:8084";

	public MyClient() {
		try {
			jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class, String.class);
		} catch (JAXBException je) {
			System.out.println("Cannot create JAXBContext " + je);
		}
	}

	// pour envoyer la requette http
	public Dispatch<Source> request(String chemin, String method) {
		service = Service.create(qname);
		service.addPort(qname, HTTPBinding.HTTP_BINDING, url + chemin);
		Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
		Map<String, Object> requestContext = dispatcher.getRequestContext();
		requestContext.put(MessageContext.HTTP_REQUEST_METHOD, method);
		return dispatcher;
	}

	// afficher l'ensemble des animaux
	public void get_animals() throws JAXBException {
		Animal animal = new Animal();
		Dispatch<Source> dispatcher = request("/animals", "GET");
		Source result = dispatcher.invoke(new JAXBSource(jc, animal));
		printSource(result);
	}

	// Ajoute un animal dans votre centre
	public void add_animal(Animal animal) throws JAXBException {
		Dispatch<Source> dispatcher = request("/animals", "POST");
		Source result = dispatcher.invoke(new JAXBSource(jc, animal));
		printSource(result);
	}

	// Supprime l'ensemble des animaux
	public void delete_animals() throws JAXBException {
		Center center = new Center();
		Dispatch<Source> dispatcher = request("/animals", "DELETE");
		Source result = dispatcher.invoke(new JAXBSource(jc, center));
		printSource(result);
	}

	// Modifie l'ensemble des animaux
	public void update_animals(Cage c) throws JAXBException {
		Dispatch<Source> dispatcher = request("/animals", "PUT");
		Source result = dispatcher.invoke(new JAXBSource(jc, c));
		printSource(result);
	}

	// Cr�e l�animal identifi� par {animal_id}
	public void create_animalWithId(Animal animal, UUID id) throws JAXBException {
		Dispatch<Source> dispatcher = request("/animals/" + id, "POST");
		Source result = dispatcher.invoke(new JAXBSource(jc, animal));
		printSource(result);
	}

	// Modifie l�animal identifi� par {animal_id}
	public void update_animal(Animal animal, String Id) throws JAXBException {
		Dispatch<Source> dispatcher = request("/animals/" + Id, "PUT");
		Source result = dispatcher.invoke(new JAXBSource(jc, animal));
		printSource(result);
	}

	// Supprime un animal
	public void delete_animal(String id) throws JAXBException {
		Animal animal = new Animal();
		Dispatch<Source> dispatcher = request("/animals/" + id, "DELETE");
		Source result = dispatcher.invoke(new JAXBSource(jc, animal));
		printSource(result);
	}

	// find un animal par son nom
	public void find_animalByName(String name) throws JAXBException {
		Center center = new Center();
		Dispatch<Source> dispatcher = request("/find/byName/" + name, "GET");
		Source result = dispatcher.invoke(new JAXBSource(jc, center));
		printSource(result);
	}

	// find un animal par sa position
	public void find_animalAtPosition(String Position) throws JAXBException {
		Center center = new Center();
		Dispatch<Source> dispatcher = request("/find/at/" + Position, "GET");
		Source result = dispatcher.invoke(new JAXBSource(jc, center));
		printSource(result);
	}

	// find un animal pr�s d'une position
	public void find_animalNearPosition(String Position) throws JAXBException {
		Center center = new Center();
		Dispatch<Source> dispatcher = request("/find/near/" + Position, "GET");
		Source result = dispatcher.invoke(new JAXBSource(jc, center));
		printSource(result);
	}
	
	// afficher les info d'un animal
		public void getAnimalWolf(UUID id) throws JAXBException {
			Center center = new Center();
			Dispatch<Source> dispatcher = request("/animals/" + id + "/wolf", "GET");
				String result = dispatcher.toString();
			System.out.printf(result);
		}

	//Am�lioration : ajouter un cage
	public void add_cage(Cage cage) throws JAXBException {
		Dispatch<Source> dispatcher = request("/cages", "POST");
		Source result = dispatcher.invoke(new JAXBSource(jc, cage));
		printSource(result);
	}

	// Am�lioration: delete les animaux d'un cage
	public void delete_animalsByCage(String cage) throws JAXBException {
		Center center = new Center();
		Dispatch<Source> dispatcher = request("/animals/byCage/" + cage, "DELETE");
		Source result = dispatcher.invoke(new JAXBSource(jc, center));
		printSource(result);
	}

	

	public void printSource(Source s) {
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(s, new StreamResult(System.out));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void main(String args[]) throws Exception {
		MyClient client = new MyClient();

		// Le S�narios

		int choice;
		UUID galago_de_Rondo_Id = UUID.randomUUID();
		UUID alpha_saiga_id = UUID.randomUUID();
		UUID alpha_spix_id = UUID.randomUUID();
		
		
		do {
			
			System.out.println("\nVeuillez saisir le numéro d'un scénario : \n");
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(System.in);
			choice = scan.nextInt();
			UUID id = UUID.randomUUID();

			// client.add_animal(new Animal("Bob", "amazon", "Arapaima gigas",id ));
			switch (choice) {

			case 1:
				client.get_animals();
				break;

			case 2:
				client.delete_animals();
				break;

			case 3:
				client.get_animals();
				break;

			case 4:
				client.add_cage(new Cage("Rouen", new Position(49.443889, 1.103333), 100, new LinkedList<>()));
				client.add_animal(new Animal("Panda", "Rouen", "spec", UUID.randomUUID()));
				break;

			case 5:
				client.add_cage(new Cage("Paris", new Position(48.856578, 2.35182), 100, new LinkedList<>()));
				client.add_animal(new Animal("Hocco unicorne", "Paris", "spec", UUID.randomUUID()));
				break;

			case 6:
				client.get_animals();
				break;

			case 7:
				client.update_animals(new Cage("Rouen", new Position(49.443889, 1.103333), 25, new LinkedList<>(Arrays
						.asList(new Animal("Lagotriche à queue jaune", "Rouen", "Chipmunk", UUID.randomUUID())))));
				break;

			case 8:
				client.get_animals();
				break;

			case 9:
				client.add_cage(new Cage("Somalie", new Position(2.333333, 48.85), 100, new LinkedList<>()));
				client.add_animal(new Animal("Océanite de Matsudaira", "Somalie", "spec", UUID.randomUUID()));
				break;

			case 10:
				client.add_animal(new Animal("Ara de Spix", "Rouen", "spec", alpha_spix_id));
				break;

			case 11:
				client.add_cage(new Cage("Bihorel", new Position(49.455278, 1.116944), 100, new LinkedList<>()));

				client.add_animal(new Animal("Galago de Rondo", "Bihorel", "spec", galago_de_Rondo_Id));
				break;

			case 12:
				client.add_cage(new Cage("Londres ", new Position(51.504872, -0.07857), 100, new LinkedList<>()));
				client.add_animal(new Animal("Palette des Sulu", "Londres", "spec", UUID.randomUUID()));
				break;

			case 13:
				client.add_animal(new Animal("Kouprey", "Paris", "spec", UUID.randomUUID()));
				break;

			case 14:
				client.add_animal(new Animal("Tuit­tuit", "Paris", "spec", UUID.randomUUID()));
				break;

			case 15:
				client.add_cage(new Cage("Canada", new Position(43.2, -80.38333), 100, new LinkedList<>()));
				client.add_animal(new Animal("Saiga", "Canada", "spec", alpha_saiga_id));
				break;

			case 16:
				client.add_cage(new Cage("Porto­Vecchio", new Position(41.5895241, 9.2627), 100, new LinkedList<>()));
				client.add_animal(new Animal("Inca de Bonaparte", "Porto­Vecchio", "spec", UUID.randomUUID()));
				break;

			case 17:
				client.get_animals();
				break;

			case 18:
				client.add_cage(new Cage("Montreux", new Position(46.4307133, 6.9113575), 100, new LinkedList<>()));
				client.add_animal(new Animal("Râle de Zapata", "Montreux", "spec", UUID.randomUUID()));
				break;

			case 19:
				client.add_cage(new Cage("Villers­Bocage", new Position(50.0218, 2.3261), 100, new LinkedList<>()));
				client.add_animal(new Animal("Rhinocéros de Java", "Villers­Bocage", "spec", UUID.randomUUID()));
				break;

			case 20:
				for (int i = 0; i < 101; i++) {
					client.add_animal(new Animal("Dalmatiens", "usa", "spec", UUID.randomUUID()));
				}
				break;

			case 21:
				client.get_animals();
				break;

			case 22:
				client.delete_animalsByCage("Paris");
				break;

			case 23:
				client.get_animals();
				break;

			case 24:
				client.find_animalByName("Galago de Rondo");
				break;

			case 25:
				client.delete_animal(galago_de_Rondo_Id.toString());
				break;

			case 26:
				client.delete_animal(galago_de_Rondo_Id.toString());
				break;

			case 27:
				client.get_animals();
				break;

			case 28:
				client.find_animalNearPosition("49.443889, 1.103333");
				break;

			case 29:
				client.find_animalAtPosition("49.443889, 1.103333");
				break;

			case 30:
				client.getAnimalWolf(alpha_saiga_id);
				break;
				
			case 31:
					client.getAnimalWolf(alpha_spix_id);
					break;
				
			case 32:
				client.get_animals();
				break;

			case 33:
				client.delete_animals();
				break;

			}
		} while (choice < 100);

	}
}