
# ZOO MANAGER (RESTFUL API)
Zoo Manager est une API Restful utilisée pour gérer un zoo
               
##  Pour commencer 
 - Exécutez MyServiceTP pour exécuter le serveur.
 - Exécutez MyClient qui va nous proposer une liste de scénario à exécuter, il suffit d'entrer le nombre de votre scénario que vous voulez dans la console de votre Ide en suivant leur ordre sur ce scénario proposé:



 1. Affichez l'ensemble des animaux
 2. Supprimez touts les animaux
 3.  Affichez l'ensemble des animaux
 4. Ajoutez un Panda à Rouen (Latitude : 49.443889 ; Longitude : 1.103333)
 5. Ajoutez un Hocco unicorne à Paris (Latitude : 48.856578 ; Longitude : 2.351828)
 6. Affichez tous les animaux
 7. Modifiez l'ensemble des animaux par un Lagotriche à queue jaune à Rouen (Latitude :49.443889 ; Longitude : 1.103333)
 8. Affichez tous les animaux
 9. Ajoutez une Océanite de Matsudaira en Somalie (Latitude : 2.333333 ; Longitude : 48.85)
 10. Ajoutez un Ara de Spix à Rouen (Latitude : 49.443889 ; Longitude : 1.103333) 
 11. Ajoutez un Galago de Rondo à Bihorel (Latitude : 49.455278 ; Longitude : 1.116944)
 12. Ajoutez une Palette des Sulu à Londres (Latitude : 51.504872 ; Longitude : -0.07857)
 13. Ajoutez un Kouprey à Paris (Latitude : 48.856578 ; Longitude : 2.351828)
 14. Ajoutez un Tuit-tuit à Paris (Latitude : 48.856578 ; Longitude : 2.351828)
 15. Ajoutez une Saïga au Canada (Latitude : 43.2 ; Longitude : -19.4435386)
 16. Ajoutez un Inca de Bonaparte à Porto-Vecchio (Latitude : 41.5895241 ; Longitude : 9.2627)
 17. Affichez l'ensemble des animaux
 18. Ajoutez un Râle de Zapata à Montreux (Latitude : 46.4307133; Longitude : 6.9113575)
 19. Ajoutez un Rhinocéros de Java à Villers-Bocage (Latitude : 50.0218 ; Longitude : 2.3261)
 20. Ajoutez 101 Dalmatiens dans une cage aux USA
 21. Affichez l'ensemble des animaux
 22. Supprimez tous les animaux de Paris
 23. Affichez l'ensemble des animaux
 24. Recherchez le Galago de Rondo
 25. Supprimez le Galago de Rondo
 26. Supprimez à nouveau le Galago de Rondo
 27. Affichez l'ensemble des animaux
 28. Affichez les animaux situés près de Rouen
 29. Affichez les animaux à Rouen
 30. Affichez les informations Wolfram Alpha du Saïga
 31. Affichez les informations Wolfram Alpha de l'Ara de Spix
 32. Supprimez tous les animaux
 33. Affichez l'ensemble des animaux
 #### Le service s'exécute sur le port 8084 par défaut
## Les fonctions du service
| METHOD | URL  | BODY|DESCRIPTION |
|--------|------|-----|------------|
| GET | /animals |  | Retourne l'ensemble des animaux du centre |
| POST| /animals | Animal |Ajoute un animal dans votre centre|
| GET| /animals/{animal_id} | Animal |Retourne l’animal identifié par {animal_id}|
| PUT| /animals ||Modifie l'ensemble des animaux|
| DELETE| /animals |  |Supprime l'ensemble des animaux|
| POST| /animals/{animal_id} | Animal |Crée l’animal identifié par {animal_id}|
| PUT| /animals/{animal_id} | Animal |Modifie l’animal identifié par {animal_id}|
| DELETE| /animals/{animal_id} | |Supprime l’animal identifié par {animal_id}|
| GET| /find/byName/{name} | |Recherche d'un animal par son nom|
| GET| /find/at/{position} | |Recherche d'un animal par position|
| GET| /find/near/{position} | |Recherche des animaux près d’une position|
| GET| /animals/{animal_id}/wolf | |Récupération des info. Wolfram d’un animal|
| GET| /center/journey/from/{position}| |Récupération des info. Du trajet depuis une position GPS jusqu’à votre centre en utilisant le service Graphhopper|

##  Pré-requis

 - Java 8
 - WolframAlpha
 - GraphHopper
 ## Auteurs
Les auteurs se trouvent dans le fichier  AUTEURS .md
